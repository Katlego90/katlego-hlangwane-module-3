// ignore_for_file: prefer_const_constructors, sort_child_properties_last

import 'package:flutter/material.dart';
import 'feature_page1.dart';
import 'login_page.dart';
import 'profile_edit_page.dart';

import 'feature_page2.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ProfileEditPage()),
              );
            },
            icon: Icon(Icons.account_circle),
          ),
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => LoginPage()),
              );
            },
            icon: Icon(
              Icons.logout,
            ),
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => FeaturePage1()));
              },
              child: Text(
                "Feature Page 1",
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.white,
                ),
              ),
              style: ElevatedButton.styleFrom(
                  primary: Colors.blueAccent,
                  onSurface: Colors.grey,
                  padding: EdgeInsets.all(20.0)),
            ),
            SizedBox(
              height: 10,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => FeaturePage2()));
              },
              child: Text("Feature Page 2",
                  style: TextStyle(fontSize: 14, color: Colors.white)),
              style: ElevatedButton.styleFrom(
                primary: Colors.blueGrey,
                onSurface: Colors.grey,
                padding: EdgeInsets.all(20.0),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ProfileEditPage(),
            ),
          );
        },
        label: const Text('Profile'),
        backgroundColor: Colors.blueAccent,
        icon: Icon(Icons.account_circle),
      ),
    );
  }
}
