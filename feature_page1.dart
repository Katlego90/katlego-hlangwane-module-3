// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class FeaturePage1 extends StatelessWidget {
  const FeaturePage1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Feature Screen 1"),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          "Feature Screen 1",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.blueGrey,
          ),
        ),
      ),
    );
  }
}
