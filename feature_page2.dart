// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class FeaturePage2 extends StatelessWidget {
  const FeaturePage2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Feature Page 2",
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          "Feature Screen 2",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.blueGrey,
          ),
        ),
      ),
    );
  }
}
